import os

import click

from .simple import sample, sort_directory, write_json
from .path_utils import append_to_filename, determine_output_directory


@click.command()
@click.argument('directory', type=click.Path(exists=True))
@click.option('--append', '-a', default='_sampled')
@click.option('--multiplier', '-m', default=1)
@click.option('--output-directory', '-o', callback=determine_output_directory)
@click.option('--verbose/--silent', default=False)
def cli(directory, append, multiplier, output_directory, verbose):
    """Randomly sample a directory of JSON files based on the minimum JSON length."""
    sorted_objs = sort_directory(directory)
    min_length = len(sorted_objs[0].contents)
    k = multiplier * min_length

    for filepath, contents in sorted_objs:
        output_contents = sample(contents, k)
        output_filename = os.path.join(output_directory, append_to_filename(append, filepath))

        write_json(output_filename, output_contents)

        if verbose:
            click.echo('sampled {0} items into {1}'.format(len(output_contents), output_filename))


if __name__ == '__main__':
    cli()
