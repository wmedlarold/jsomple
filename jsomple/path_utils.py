from distutils.dir_util import mkpath
import os


DEFAULT_OUTPUT_DIRECTORY = 'output'


def determine_output_directory(context, param, value):
    """Determine the output directory based on the input directory."""
    input_directory = context.params.get('directory')

    if value is None:
        value = os.path.join(input_directory, DEFAULT_OUTPUT_DIRECTORY)

    # build full directory path
    mkpath(value)

    return value


def directory_listing(dirpath):
    """List files in a directory prefixed by the directory name."""
    path = os.path.abspath(dirpath)
    for filename in os.listdir(path):
        yield os.path.join(path, filename)


def append_to_filename(text, filepath):
    """Append `text` to the end of `filepath`, but before its extension."""
    filename, ext = os.path.splitext(os.path.basename(filepath))
    return filename + text + ext