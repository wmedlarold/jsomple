from collections import namedtuple
import json
import random

from .path_utils import directory_listing


JSONFile = namedtuple('JSONFile', ['filepath', 'contents'])


def is_json_file(filename):
    """Casually determines if a file is a JSON file."""
    return filename.lower().endswith('.json')


def load_json(filename):
    """Load the contents of a JSON file."""
    with open(filename) as fp:
        data = json.load(fp)

    return data


def write_json(filename, json_data):
    """Write `json_data` to a file with name `filename`."""
    with open(filename, 'w+') as fp:
        json.dump(json_data, fp)


def sort_directory(dirpath):
    """Sorts a directory of JSON files by their array length."""
    filepaths = filter(is_json_file, directory_listing(dirpath))
    json_files = (JSONFile(filepath, load_json(filepath)) for filepath in filepaths)
    key_func = lambda obj: len(obj.contents)
    return sorted(json_files, key=key_func)


def sample(array, n=1):
    """Randomly sample `n` objects from an array, or the entire array if shorter than `n` items."""
    k = min(len(array), n)
    return random.sample(array, k)