from distutils.core import setup

setup(
    name='jsomple',
    version='1.1',
    description='Randomly sample from a directory of JSON files!',
    author='Will Medlar',
    author_email='will.medlar@rewardstyle.com',
    packages=['jsomple'],
    install_requires=[
        'click==6.6',
    ],
    entry_points={
        'console_scripts': [
            'jsomple = jsomple.cli:cli'
        ]
    },
)
